package cvut.fel.wordguess.domain;


import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import cvut.fel.wordguess.domain.utils.Converters;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(indices = {@Index("username")})
@NoArgsConstructor

public class Profile {

    @PrimaryKey
    @Getter
    @Setter
    @NonNull
    @ColumnInfo(name = "username")
    private String username;

    @Getter
    @NonNull
    @Setter
    @ColumnInfo(name = "password")
    private String password;

    @Getter
    @NonNull
    @Setter
    @ColumnInfo(name = "name")
    private String name;

    @Getter
    @Setter
    @NonNull
    @ColumnInfo(name = "surname")
    private String surname;

    @Getter
    @Setter
    @Nullable
    @ColumnInfo(name = "userpic_id")
    private String userpicPath;

    @Getter
    @Setter
    @ColumnInfo(name = "date_reg")
    @TypeConverters({Converters.class})
    private Date dateRegistered;

    @Override
    public String toString() {
        return String.format(
                "%s (%s %s)",
                username, name, surname
        );
    }
}
