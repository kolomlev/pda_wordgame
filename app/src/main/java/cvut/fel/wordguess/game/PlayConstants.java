package cvut.fel.wordguess.game;

public class PlayConstants {

    public static final int TIME_LIMIT_MS = 4000;
    public static final int TICK_INTERVAL = TIME_LIMIT_MS / 100;
    public static final int NEXT_QUESTION_DELAY = 2000;

    public static final int MSG_TICK = 0;
    public static final int MSG_TIMEOUT = 1;
    public static final int MSG_TIMESTOP = 2;
    public static final int MSG_EXIT_PLAY = 3;
    public static final int MSG_ANSWER = 4;
    public static final int MSG_REGISTER = 5;

}
