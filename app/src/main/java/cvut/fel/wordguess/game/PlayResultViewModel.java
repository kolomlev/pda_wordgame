package cvut.fel.wordguess.game;

import android.app.Application;
import android.util.Log;

import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import cvut.fel.wordguess.domain.Game;
import cvut.fel.wordguess.domain.Word;
import cvut.fel.wordguess.domain.db.MiscRepository;
import lombok.Getter;
import lombok.Setter;

public class PlayResultViewModel extends AndroidViewModel {
    private final String TAG = PlayResultViewModel.class.getSimpleName();


    @Setter
    @Getter
    private List<Word> rightWords;


    @Setter
    @Getter
    private List<Word> wrongWords;

    @Getter
    @Setter
    private String username;


    private MiscRepository miscRepository;


    public PlayResultViewModel(@NonNull Application application) {
        super(application);
        miscRepository = new MiscRepository(this.getApplication());
    }

    void createAndPersistGame() {
        Game g = Game.createGame(new Date(), username,
                rightWords.size() + wrongWords.size(), rightWords.size());
        Log.d(TAG, "Game created: " + g.toString());
        miscRepository.insertGame(g);

    }

}
