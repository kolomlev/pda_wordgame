package cvut.fel.wordguess.domain;


import java.io.Serializable;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import cvut.fel.wordguess.domain.utils.Converters;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity(foreignKeys =
        @ForeignKey(entity = Profile.class,
                parentColumns = "username",
                childColumns = "user")
)
@NoArgsConstructor
public class Game implements Serializable {

    @Getter
    @Setter
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;

    @Getter
    @Setter
    @TypeConverters({Converters.class})
    @NonNull
    private Date date;

    @Getter
    @Setter
    @NonNull
    private String user;

    @Getter
    @Setter
    @Nullable
    private Integer wordsPlayed;

    @Getter
    @Setter
    @Nullable
    private Integer wordsGuessed;


    public static Game createGame(Date date, String user, int wordsPlayed, int wordsGuessed) {
        Game g = new Game();
        g.setDate(date);
        g.setUser(user);
        g.setWordsPlayed(wordsPlayed);
        g.setWordsGuessed(wordsGuessed);
        return g;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", date=" + date +
                ", user='" + user + '\'' +
                ", wordsPlayed=" + wordsPlayed +
                ", wordsGuessed=" + wordsGuessed +
                '}';
    }
}
