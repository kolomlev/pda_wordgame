package cvut.fel.wordguess;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cvut.fel.wordguess.domain.Profile;
import cvut.fel.wordguess.profile.ProfileListAdapter;
import lombok.Getter;

public class StatsFragment extends Fragment {
    private static final String TAG = "StatsFragment";

    // This viewmodel is alive as long as the MainActivity is
    @Getter
    private StatsViewModel mStatsViewModel;

    private Button btName;
    private Button btScore;

    private boolean reverseSortName = false;
    private boolean reverseSortScore = false;

    private List<Profile> profiles;
    private ProfileListAdapter adapter;

    public static StatsFragment newInstance() {
        return new StatsFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stats_fragment, container, false);
        initButtons(view);

        RecyclerView recyclerView = view.findViewById(R.id.stats_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        adapter = new ProfileListAdapter(this);
        recyclerView.setAdapter(adapter);

        mStatsViewModel = ViewModelProviders.of(getActivity()).get(StatsViewModel.class);
        mStatsViewModel.getAllProfiles()
                .observe(this, profiles -> {
                    Log.d(TAG, "current users: " + profiles.toString());
                    this.profiles = profiles;
                    adapter.setmProfiles(profiles);
                });

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void initButtons(View view) {
        btName = view.findViewById(R.id.stats_bt_name);
        btName.setOnClickListener(l -> {
            // Sort current profiles by name
            Collections.sort(profiles, (p1, p2) ->
                    p1.getName().compareToIgnoreCase(p2.getName())
                            * (reverseSortName ? -1 : 1)
            );
            reverseSortName = !reverseSortName;
            adapter.setmProfiles(profiles);
        });

        btScore = view.findViewById(R.id.stats_bt_score);
        btScore.setOnClickListener(l -> {
            Collections.sort(profiles, (p1, p2) ->
                    -mStatsViewModel.getScoresForUsername(p1.getUsername()) +
                            mStatsViewModel.getScoresForUsername(p2.getUsername())
                                    * (reverseSortScore ? -1 : 1)
            );

            reverseSortScore = !reverseSortScore;
            adapter.setmProfiles(profiles);
        });
    }

    public void handleProfileItemClick(Profile p) {
        // omfg it's beautiful
        NavDirections action =
                StatsFragmentDirections.actionStatsFragmentToProfileFragment(p.getUsername());
        NavHostFragment.findNavController(this).navigate(action);

    }
}
