package cvut.fel.wordguess.game;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;
import cvut.fel.wordguess.R;

public class PlayActivity extends AppCompatActivity {
    private final String TAG = this.getClass().getSimpleName();

    private PlayViewModel playViewModel;

    // Views
    private TextView wordTV;
    private TextView definitionTV;
    private SeekBar timeBar;
    private List<Button> buttonList;
    private TextView wrongTV;
    private TextView rightTV;
    private Button endBt;

    private Handler handler;
    private PlayQuestion currentQuestion;

    private Random random = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        playViewModel = ViewModelProviders.of(this).get(PlayViewModel.class);
        initViews();
        initHandler();
        setEventListenersForButtons();


        start();
    }

    // Entry point
    private void start() {
        initWordBase();

        // Start game loop, when words are fetched
        playViewModel.getWordBase().observe(this, words -> {
            playViewModel.registerWordBase(words);
            Log.d(TAG, "Words ready");
            Log.d(TAG, "word base: " + words);
            initGameLoop();
        });
    }


    // Retrieves params from intent and sets up viewmodel
    private void initWordBase() {

        String[] diffs = getIntent().getStringArrayExtra("diffs");
        String[] tags = getIntent().getStringArrayExtra("tags");

        playViewModel.registerWordBaseParams(diffs, tags);
    }

    // Creates and posts first question runnable
    private void initGameLoop() {
        handler.postDelayed(new NextQuestionRunnable(), PlayConstants.TICK_INTERVAL);
    }


    private void initHandler() {
        handler = new Handler(Looper.getMainLooper()) {

            NextQuestionRunnable questionRunnable;
            boolean answered = false;

            @Override
            public void handleMessage(Message msg) {

                if (msg.what == PlayConstants.MSG_EXIT_PLAY) {
                    handler.removeCallbacks(questionRunnable);
                    if (questionRunnable != null) handler.removeCallbacks(questionRunnable.timer);
                    quitPlay();
                } else if (msg.what == PlayConstants.MSG_TICK) {

                    // Progress bar (time bar) editing every tick
                    timeBar.setProgress(timeBar.getProgress() - 1);

                } else if (msg.what == PlayConstants.MSG_REGISTER) {
                    // Register the question
                    questionRunnable = (NextQuestionRunnable) msg.obj;

                } else {
                    if (msg.what == PlayConstants.MSG_TIMEOUT) {
                        // TLE
                        wrongTV.setText("Wrong: " + playViewModel.getWrongCnt());
                        playViewModel.addQuestionRecord(currentQuestion, false);

                    } else if (msg.what == PlayConstants.MSG_ANSWER) {
                        Button b = (Button) msg.obj;
                        // Judging latest question if it has not been judged yet
                        if (!playViewModel.getPlayedWordsTable().containsKey(currentQuestion.getWord())) {
                            if (currentQuestion.answer(b.getText().toString())) {
                                b.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.colorCorrect));
                                playViewModel.addQuestionRecord(currentQuestion, true);
                                rightTV.setText("Correct: " + playViewModel.getRightCnt());
                            } else {
                                b.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.colorAccent));
                                playViewModel.addQuestionRecord(currentQuestion, false);
                                wrongTV.setText("Wrong: " + playViewModel.getWrongCnt());
                            }
                        }
                    }
                    wordTV.setText(currentQuestion.getWord().getSelf()); // Let see word
                    handler.removeCallbacks(questionRunnable.timer); // Stop timer
                    handler.removeCallbacks(questionRunnable); // Kill latest question
                    questionRunnable = new NextQuestionRunnable(); // Create new question
                    handler.postDelayed(questionRunnable, PlayConstants.NEXT_QUESTION_DELAY); // Run it after short delay
                }


                super.handleMessage(msg);
            }
        };
    }

    private void quitPlay() {
        // Getting current user
        String username = getIntent().getStringExtra("username");

        Intent goToResult = new Intent(this, PlayResultActivity.class);
        goToResult.putExtra("result_right", playViewModel.getRightList());
        goToResult.putExtra("result_wrong", playViewModel.getWrongList());
        goToResult.putExtra("username", username);

        finish();
        startActivity(goToResult);

    }


    // Renders the question and options for the question
    private void prepareLayout(PlayQuestion question) {

        for (int i = 0; i < buttonList.size(); i++) {
            Button b = buttonList.get(i);
            b.setText(question.getOptions().get(i));
            b.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.colorButtonDefault));

        }

        int correctButtonIndex = random.nextInt(4);
        definitionTV.setText(question.getWord().getDefinition());
        buttonList.get(correctButtonIndex).setText(question.getWord().getSelf());
    }


    private void initViews() {
        wordTV = findViewById(R.id.play_word);
        definitionTV = findViewById(R.id.play_definition);
        timeBar = findViewById(R.id.play_progressbar);
        Button opt1 = findViewById(R.id.play_bt1);
        Button opt2 = findViewById(R.id.play_bt2);
        Button opt3 = findViewById(R.id.play_bt3);
        Button opt4 = findViewById(R.id.play_bt4);
        buttonList = Arrays.asList(opt1, opt2, opt3, opt4);
        wrongTV = findViewById(R.id.play_wrong_cnt);
        rightTV = findViewById(R.id.play_right_cnt);
        endBt = findViewById(R.id.play_end);

    }


    private void setEventListenersForButtons() {
        for (Button b : buttonList) {
            b.setOnClickListener(l -> {
                handleButtonClick(b);
            });
        }

        endBt.setOnClickListener(v -> {
            Message m = handler.obtainMessage();
            m.what = PlayConstants.MSG_EXIT_PLAY;
            handler.sendMessageAtFrontOfQueue(m);
        });
    }


    private void handleButtonClick(Button b) {
        // Send answer msg
        Message m = handler.obtainMessage();
        m.what = PlayConstants.MSG_ANSWER;
        m.obj = b;
        handler.sendMessage(m);
    }

    class TimerRunnable implements Runnable {

        private int timeGone = 0;

        @Override
        public void run() {
            timeGone += PlayConstants.TICK_INTERVAL;
            Message m = handler.obtainMessage();
            if (timeGone >= PlayConstants.TIME_LIMIT_MS) {
                m = getKillMsg();
            } else {
                m.what = PlayConstants.MSG_TICK;
            }

            handler.sendMessage(m);
            handler.postDelayed(this, PlayConstants.TICK_INTERVAL);
        }

        Message getKillMsg() {
            Message m = handler.obtainMessage();
            m.what = PlayConstants.MSG_TIMEOUT;
            m.obj = this;
            return m;
        }

    }

    class NextQuestionRunnable implements Runnable {
        private final String TAG = getClass().getSimpleName();

        private TimerRunnable timer;


        @Override
        public void run() {

            // Gets next question from VM
            currentQuestion = playViewModel.getNextQuestion();

            // No words left
            if (currentQuestion == null) {
                Message m = handler.obtainMessage();
                m.what = PlayConstants.MSG_EXIT_PLAY;
                handler.sendMessageAtFrontOfQueue(m);
                return;
            }

            // Register at handler to be handled
            registerAtHandler();

            // Init question's attrs
            timeBar.setProgress(100);
            wordTV.setText("");
            this.timer = new TimerRunnable();

            // A little delay before countdown start
            handler.postDelayed(this.timer, PlayConstants.TICK_INTERVAL);

            prepareLayout(currentQuestion);
        }


        void registerAtHandler() {
            Message m = handler.obtainMessage();
            m.what = PlayConstants.MSG_REGISTER;
            m.obj = this;
            handler.sendMessageAtFrontOfQueue(m);
        }
    }

}
