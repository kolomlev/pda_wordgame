package cvut.fel.wordguess;

import android.app.Application;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import cvut.fel.wordguess.domain.Quote;
import cvut.fel.wordguess.domain.db.MiscRepository;
import cvut.fel.wordguess.domain.db.retrofitApi.QuoteApi;
import cvut.fel.wordguess.domain.utils.DateUtils;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class HomeViewModel extends AndroidViewModel {

    private MutableLiveData<Quote> quote;
    private static Retrofit retrofit;
    private final String BASE_URL = "https://quotes.rest/";

    private MiscRepository miscRepository;

    public HomeViewModel(@NonNull Application application) {
        super(application);
        miscRepository = new MiscRepository(this.getApplication());
    }

    // Caching the quote to reduce the request count
    public LiveData<Quote> getQuote() {

        if (quote == null) {
            quote = new MutableLiveData<>();
            Quote todayQuote = tryGetTodayQuote();
            if (todayQuote == null) {
                loadQuote();
            } else {
                quote.postValue(todayQuote); //post to inform UI
            }
        }
        return quote;
    }


    private Quote tryGetTodayQuote() {
        try {
            Quote latest = miscRepository.getLatestQuote();
            if (latest == null) return null;
            if (DateUtils.isToday(latest.getDateAdded())) {
                return latest;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        return null;
    }

    private void loadQuote() {

        retrofit = new Retrofit.Builder().baseUrl(BASE_URL)
                .build();
        QuoteApi api = retrofit.create(QuoteApi.class);
        Call<ResponseBody> call = api.loadQuote();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                Log.d("retrofit", "response");
                try {
                    assert response.body() != null;
                    JSONObject quoteRaw = new JSONObject(response.body().string())
                            .getJSONObject("contents")
                            .getJSONArray("quotes")
                            .getJSONObject(0);
                    String author = quoteRaw.getString("author");
                    String content = quoteRaw.getString("quote");

                    Quote quoteObj = new Quote();
                    quoteObj.setAuthor(author);
                    quoteObj.setContent(content);
                    quoteObj.setDateAdded(new Date());

                    persistQuote(quoteObj);

                    quote.postValue(quoteObj);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }

            private void persistQuote(Quote q) {
                miscRepository.insertQuote(q);
            }
        });
    }

}
