package cvut.fel.wordguess.domain.db;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.lifecycle.LiveData;
import cvut.fel.wordguess.domain.Profile;

public class ProfileRepository {

    private AppDatabase db;

    public ProfileRepository(Context context) {
        db = AppDatabase.getDatabase(context);
    }

    @SuppressLint("StaticFieldLeak")
    public void insertProfile(final Profile p) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                db.profileModel().insert(p);
                return null;
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    public Profile getUserByUsernameSync(String username) throws ExecutionException, InterruptedException {
        return new AsyncTask<Void, Void, Profile>() {
            @Override
            protected Profile doInBackground(Void... voids) {
                return db.profileModel().getUserByUsernameSync(username);
            }
        }.execute().get();
    }



    public LiveData<Profile> getUserByUsername(String username) {
        return db.profileModel().getUserByUsername(username);
    }

    public LiveData<List<Profile>> getAllUsers() {
        return db.profileModel().getAllUsers();
    }

    @SuppressLint("StaticFieldLeak")
    public void updateUser(Profile user) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                db.profileModel().updateUser(user);
                return null;
            }
        }.execute();

    }

    public void deleteUser(Profile user){
        db.profileModel().deleteUser(user);
    }
}
