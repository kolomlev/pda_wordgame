package cvut.fel.wordguess.domain;


import java.io.Serializable;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import cvut.fel.wordguess.domain.utils.Converters;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity(foreignKeys = {
        @ForeignKey(entity = Profile.class,
                parentColumns = "username",
                childColumns = "added_by"),
        @ForeignKey(entity = Tag.class,
                parentColumns = "name",
                childColumns = "tag",
                onDelete = ForeignKey.CASCADE)
},
        indices = {@Index("added_by"), @Index("tag")})
@NoArgsConstructor
public class Word implements Serializable {

    @PrimaryKey
    @Getter
    @Setter
    @NonNull
    @ColumnInfo(name = "word")
    private String self;

    @Getter
    @Setter
    @NonNull
    @ColumnInfo(name = "definition")
    private String definition;

    @NonNull
    @Getter
    @Setter
    @TypeConverters({Difficulty.class})
    private Difficulty difficulty;

    @Nullable
    @Getter
    @Setter
    @TypeConverters({Converters.class})
    @ColumnInfo(name = "date_added")
    private Date dateAdded;

    @Nullable
    @Getter
    @Setter
    @ColumnInfo(name = "added_by")
    private String addedBy;

    @Nullable
    @Getter
    @Setter
    private String tag;


    @NonNull
    @Override
    public String toString() {
        return String.format("%s: %s. Difficulty: %s; Tag: %s; Added on %s by: %s", self, definition,
                difficulty.toString(), tag, dateAdded != null ? dateAdded.toString() : "<null>",
                addedBy != null ? addedBy : "<null>");
    }

    public static Word create(String self, String definition, Difficulty difficulty,
                              Date dateAdded, Profile addedBy, Tag tag) {
        Word w = new Word();
        w.setSelf(self);
        w.setDefinition(definition);
        w.setTag(tag != null ? tag.getName() : null);
        w.setDifficulty(difficulty);
        w.setDateAdded(dateAdded);
        w.setAddedBy(addedBy != null ? addedBy.getUsername() : null);
        return w;
    }
}
