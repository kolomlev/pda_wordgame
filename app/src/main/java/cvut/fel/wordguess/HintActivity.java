package cvut.fel.wordguess;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;

public class HintActivity extends AppCompatActivity {

    private Button bt;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hint);


        // Set first start happened
        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("firstStart", false);
        editor.apply();

        bt = findViewById(R.id.hint_bt);
        bt.setOnClickListener(l -> {
            Intent goToMain = new Intent(this, MainActivity.class);
            goToMain.putExtra("username", getIntent().getStringExtra("username"));
            startActivity(goToMain, ActivityOptions.makeBasic().toBundle());
        });
    }


}
