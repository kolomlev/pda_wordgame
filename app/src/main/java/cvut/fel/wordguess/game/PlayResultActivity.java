package cvut.fel.wordguess.game;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cvut.fel.wordguess.MainActivity;
import cvut.fel.wordguess.R;
import cvut.fel.wordguess.dictionary.DictionaryItemListAdapter;
import cvut.fel.wordguess.domain.Word;
import cvut.fel.wordguess.domain.utils.WordListWrapper;

public class PlayResultActivity extends AppCompatActivity {
    private final String TAG = getClass().getSimpleName();

    private Button btDismiss;
    private Button btShare;

    private TextView cntRightTV;
    private TextView cntTotalTV;

    private RecyclerView rightRV;
    private RecyclerView wrongRV;


    private PlayResultViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_result);
        initViews();

        viewModel = ViewModelProviders.of(this).get(PlayResultViewModel.class);

        setResultWordsAndUsername();
        viewModel.createAndPersistGame();
        renderResults();
        initRecyclers();
    }

    // Render game results
    private void renderResults() {
        cntTotalTV.setText(String.valueOf(viewModel.getRightWords().size() + viewModel.getWrongWords().size()));
        cntRightTV.setText(String.valueOf(viewModel.getRightWords().size()));
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void initViews() {
        btDismiss = findViewById(R.id.res_bt_dismiss);
        btShare = findViewById(R.id.res_bt_share);
        cntRightTV = findViewById(R.id.res_cnt_right);
        cntTotalTV = findViewById(R.id.res_cnt_total);

        btShare.setOnClickListener(c -> {

        });

        btDismiss.setOnClickListener(c -> {
            Intent goHome = new Intent(this, MainActivity.class);
            goHome.putExtra("username", viewModel.getUsername());
            startActivity(goHome, ActivityOptions.makeBasic().toBundle());
        });


    }


    private void setResultWordsAndUsername() {

        ArrayList<Word> right = ((WordListWrapper) getIntent()
                .getSerializableExtra("result_right")).getWords();
        ArrayList<Word> wrong = ((WordListWrapper) getIntent()
                .getSerializableExtra("result_wrong")).getWords();
        String username = getIntent().getStringExtra("username");

//        Log.d(TAG, "Username passed is: " + username);
        viewModel.setRightWords(right);
        viewModel.setWrongWords(wrong);
        viewModel.setUsername(username);
    }

    private void initRecyclers() {
        // Init right words recycler
        rightRV = findViewById(R.id.res_recycler_right);
        rightRV.setLayoutManager(new LinearLayoutManager(this));
        DictionaryItemListAdapter adapter = new DictionaryItemListAdapter();
        adapter.setBackground(Color.argb(150, 20, 200, 0));
        rightRV.setAdapter(adapter);
        adapter.setWords(viewModel.getRightWords());

        // Init wrong words recycler
        wrongRV = findViewById(R.id.res_recycler_wrong);
        wrongRV.setLayoutManager(new LinearLayoutManager(this));
        DictionaryItemListAdapter adapter2 = new DictionaryItemListAdapter();
        adapter2.setBackground(Color.argb(150, 200, 0, 20));
        wrongRV.setAdapter(adapter2);
        adapter2.setWords(viewModel.getWrongWords());


    }
}
