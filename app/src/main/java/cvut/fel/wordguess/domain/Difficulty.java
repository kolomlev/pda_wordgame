package cvut.fel.wordguess.domain;

import androidx.room.TypeConverter;
import lombok.Getter;

public enum Difficulty {
    Beginner(0),
    Intermediate(1),
    Advanced(2),
    All(3);

    @Getter
    private final Integer code;

    Difficulty(Integer value) {
        this.code = value;
    }

    @TypeConverter
    public static Difficulty getDifficulty(Integer code){
        for (Difficulty d : values()) {
            if (d.code.equals(code)) {
                return d;
            }
        }
        return null;
    }

    @TypeConverter
    public static Integer getCode(Difficulty d){
        return d== null ? null : d.code;
    }
}
