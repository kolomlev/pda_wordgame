package cvut.fel.wordguess.domain.db.dao;


import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import cvut.fel.wordguess.domain.Game;

@Dao
public interface GameDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Game game);

    @Query("select * from Game")
    LiveData<List<Game>> getAllGames();



    @Query("select * from Game g where g.user = :username")
    List<Game> getAllGamesForUsernameSync(String username);


    @Query("select * from Game g where g.user = :username")
    LiveData<List<Game>> getAllGamesForUsernameAsync(String username);



    @Update
    void updateGame(Game game);

    @Delete
    void deleteGame(Game game);
}
