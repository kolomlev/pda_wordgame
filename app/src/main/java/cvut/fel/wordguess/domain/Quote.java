package cvut.fel.wordguess.domain;

import java.util.Date;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
public class Quote {

    @Getter
    @Setter
    @PrimaryKey(autoGenerate = true)
    private int id;

    @Getter
    @Setter
    private String author;

    @Getter
    @Setter
    private String content;

    @Getter
    @Setter
    private Date dateAdded;

}
