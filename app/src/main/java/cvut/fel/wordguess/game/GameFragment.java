package cvut.fel.wordguess.game;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import cvut.fel.wordguess.R;
import cvut.fel.wordguess.domain.Tag;

public class GameFragment extends Fragment {
    private final String TAG = this.getClass().getSimpleName();
    public static final int REQ_CODE = 1;

    private GameViewModel viewModel;

    private ChipGroup chipGroup;
    private Button random;
    private Button play;
    private CheckBox chbxBeginner;
    private CheckBox chbxInter;
    private CheckBox chbxAdv;

    // Remember chosen tags for latter passing Tags to game activity
    private Map<Chip, Tag> lookup = new HashMap<>();

    public static GameFragment newInstance() {
        return new GameFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        // Init views
        View v = inflater.inflate(R.layout.game_fragment, container, false);

        initViews(v);

        viewModel = ViewModelProviders.of(this).get(GameViewModel.class);

        initTags();
        initButtons();

        /* TESTING GAMES LIST */

        viewModel.getCurrentGames().observe(this, list -> {
            Log.d(TAG, "Current games: " + list.toString());
        });
        return v;
    }

    private void initViews(View v) {
        chipGroup = v.findViewById(R.id.game_tags);
        random = v.findViewById(R.id.game_randomize);
        play = v.findViewById(R.id.game_play);
        chbxBeginner = v.findViewById(R.id.game_chbx_beginner);
        chbxInter = v.findViewById(R.id.game_chbx_inter);
        chbxAdv = v.findViewById(R.id.game_chbx_adv);
    }


    private void initTags() {

        for (Tag tag : viewModel.getCurrentTags()) {

            Chip c = new Chip(this.getContext());
            c.setText(tag.getName());
            c.setId(View.generateViewId());
            c.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            c.setCheckable(true);
            chipGroup.addView(c);
            lookup.put(c, tag);
        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    private void initButtons() {
        // List checkboxes for convenience
        List<CheckBox> checkBoxes = new ArrayList<>();
        checkBoxes.add(chbxBeginner);
        checkBoxes.add(chbxInter);
        checkBoxes.add(chbxAdv);


        /* Set up random button */
        // Randomly choose difficulty and chipGroup
        random.setOnClickListener(click -> {
            checkBoxes.forEach(c ->
                    c.setChecked(
                            Math.random() >= 0.5d)
            );
            for (int i = 0; i < chipGroup.getChildCount(); i++) {
                Chip c = (Chip) chipGroup.getChildAt(i);
                c.setChecked(
                        Math.random() >= 0.5d
                );
            }
        });

        /* Set up new game starter*/
        play.setOnClickListener(click -> {
            // Get difficulty and chosen chipGroup
            String[] diffs = checkBoxes.stream().filter(CompoundButton::isChecked)
                    .map(TextView::getText)
                    .map(CharSequence::toString).toArray(String[]::new);
            String[] tags = lookup.entrySet().stream()
                    .filter(chipTagEntry -> chipTagEntry.getKey().isChecked())
                    .map(Map.Entry::getValue)
                    .map(Tag::getName)
                    .toArray(String[]::new);

            String username = getActivity().getIntent().getStringExtra("username");

            Intent goPlay = new Intent(this.getContext(), PlayActivity.class);
            goPlay.putExtra("diffs", diffs);
            goPlay.putExtra("tags", tags);
            goPlay.putExtra("username", username);

            // Launch game
            startActivity(goPlay, ActivityOptions.makeBasic().toBundle());
        });
    }
}
