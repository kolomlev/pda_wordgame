package cvut.fel.wordguess.auth;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import cvut.fel.wordguess.R;
import cvut.fel.wordguess.domain.Profile;
import cvut.fel.wordguess.domain.db.ProfileRepository;
import cvut.fel.wordguess.domain.utils.Validation;

public class RegisterActivity extends AppCompatActivity {

    // We can have DB instance here, since it's just registration activity
    private ProfileRepository profileRepository;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        profileRepository = new ProfileRepository(this);
    }

    public void navigateToLogin(View link) {
        Intent goToLog = new Intent(this, LoginActivity.class);
        startActivity(goToLog);
    }

    private void goBackToLogin(String username) {
        final Intent goBackToLog = new Intent(this, LoginActivity.class);

        goBackToLog.putExtra("username", username);
        (new Handler())
                .postDelayed(
                        () -> startActivity(goBackToLog), Toast.LENGTH_SHORT + 500);
    }

    public void attemptRegister(View button) throws NoSuchAlgorithmException, ExecutionException, InterruptedException {

        // Getting all the views
        TextInputLayout usernameField = findViewById(R.id.register_layout_username);
        TextInputEditText usernameContent = findViewById(R.id.register_username);
        String username = Objects.requireNonNull(usernameContent.getText()).toString();

        TextInputLayout passwordField = findViewById(R.id.register_layout_password);
        TextInputEditText passwordContent = findViewById(R.id.register_password);
        String password = Objects.requireNonNull(passwordContent.getText()).toString();

        TextInputLayout nameField = findViewById(R.id.register_layout_name);
        TextInputEditText nameContent = findViewById(R.id.register_name);
        String name = Objects.requireNonNull(nameContent.getText()).toString();

        TextInputLayout surnameField = findViewById(R.id.register_layout_surname);
        TextInputEditText surnameContent = findViewById(R.id.register_surname);
        String surname = Objects.requireNonNull(surnameContent.getText()).toString();

        // Validating fields
        if (Validation.validateUsername(usernameContent, usernameField) &&
                Validation.validatePassword(passwordContent, passwordField) &&
                Validation.validateName(nameContent, nameField) &&
                Validation.validateSurname(surnameContent, surnameField)) {


            // Using "sync" get via AsyncTask rather than LiveData
            Profile attemptProfile = profileRepository.getUserByUsernameSync(username);
            System.out.println(attemptProfile);
            // Fields OK, check uniqueness
            if (attemptProfile != null) {
                // Username already taken

                Toast.makeText(this, "This username is already taken",
                        Toast.LENGTH_SHORT).show();

                usernameContent.setText("");
                passwordContent.setText("");
                nameContent.setText("");
                surnameContent.setText("");

            } else {
                // Inserting new user to db
                Profile newUser = createNewProfile(username, password, name, surname, new Date());
                profileRepository.insertProfile(newUser);

                // Going back to login activity after toast is shown
                Toast.makeText(this, "User " + username + " successfully registered",
                        Toast.LENGTH_SHORT).show();
                goBackToLogin(username);
            }
        }

    }

    private Profile createNewProfile(String username, String password, String name, String surname, Date date) throws NoSuchAlgorithmException {
        // All OK, create new user
        Profile newUser = new Profile();
        newUser.setUsername(username);

        //Storing hash instead of plaintext
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(password.getBytes());
        String encrypted = new String(messageDigest.digest());
        newUser.setPassword(encrypted);

        newUser.setName(name);
        newUser.setSurname(surname);
        newUser.setDateRegistered(date);

        return newUser;
    }


}
