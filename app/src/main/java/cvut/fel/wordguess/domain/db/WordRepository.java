package cvut.fel.wordguess.domain.db;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import cvut.fel.wordguess.domain.Word;

public class WordRepository {

    private AppDatabase db;

    public WordRepository(Context context) {db = AppDatabase.getDatabase(context);}

    @SuppressLint("StaticFieldLeak")
    public void insertWord(final Word w) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                Log.d("WordRepo", w.toString());
                db.wordModel().insert(w);
                return null;
            }
        }.execute();
    }




}
