package cvut.fel.wordguess.domain.db.retrofitApi;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

public interface QuoteApi {
    @GET("/qod")
    Call<ResponseBody> loadQuote();
}
