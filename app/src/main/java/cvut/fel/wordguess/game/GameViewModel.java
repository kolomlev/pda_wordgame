package cvut.fel.wordguess.game;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import cvut.fel.wordguess.domain.Game;
import cvut.fel.wordguess.domain.Tag;
import cvut.fel.wordguess.domain.db.MiscRepository;

public class GameViewModel extends AndroidViewModel {

    private List<Tag> currentTags = new ArrayList<>();

    private MiscRepository miscRepository;

    private LiveData<List<Game>> allGames;


    public GameViewModel(@NonNull Application application) {
        super(application);
        miscRepository = new MiscRepository(this.getApplication());

    }

    // Queries db each time in case tags changed (should not change though since user does not add them)
    List<Tag> getCurrentTags() {
        try {
            currentTags = miscRepository.getAllTags();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return currentTags;
    }

    LiveData<List<Game>> getCurrentGames() {
        if (allGames == null) {
            allGames = miscRepository.getAllGames();
        }

        return allGames;
    }


}
