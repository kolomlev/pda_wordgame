package cvut.fel.wordguess.domain.db.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import cvut.fel.wordguess.domain.Difficulty;
import cvut.fel.wordguess.domain.Word;

//TODO: edit the methods when the class model is done
@Dao
public interface WordDao {

    @Query("delete from Word ")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Word word);


    @Query("select * from Word limit (:n)")
    LiveData<List<Word>> getNWords(int n );

    /* SORTED*/


    @Query("select * from Word order by word.word asc")
    LiveData<List<Word>> getAllWordsSorted();


    @Query("select * from Word where word.tag is null and word.difficulty in (:diffs) order by word.word asc")
    LiveData<List<Word>> getUntaggedWordsSorted(List<Difficulty> diffs);



    @Query("select * from Word where (word.tag is null or word.tag in (:tagNames)) " +
            "and word.difficulty in (:diffs) order by word.word asc")
    LiveData<List<Word>> getUntaggedAndTaggedWordsSorted(List<Difficulty> diffs, List<String> tagNames);


    @Query("select * from Word " +
            "where word.difficulty in (:diffs) " +
            "order by word.word ASC")
    LiveData<List<Word>> getAllWordsByDifficultiesListSorted(List<Difficulty> diffs);


    @Query("select * from Word " +
            "where word.tag in (:tagNames) and word.difficulty in (:diffs) " +
            "order by word.word ASC")
    LiveData<List<Word>> getTaggedWordsSorted
                            (List<String> tagNames, List<Difficulty> diffs);



//    /* RANDOM */
//    @Query("select * from Word " +
//            "where word.tag in (:tagNames) and word.difficulty in (:diffs) " +
//            "order by random() limit :n")
//    LiveData<List<Word>> getNRandomWordsFilteredByTagNamesListAndDifficultiesList
//                            (List<Difficulty>diffs, List<String> tagNames, int n);
//
//    @Query("select * from Word " +
//            "where word.difficulty in (:diffs) and word.tag is null " +
//            "order by random() limit :n")
//    LiveData<List<Word>> getNRandomUntaggedWordsFilteredByDifficultiesList
//                            (List<Difficulty> diffs, int n);
//
//    @Query("select * from Word " +
//            "where word.difficulty in (:diffs) and (word.tag in (:tagNames) or word.tag is null)" +
//            "order by random() limit :n")
//    LiveData<List<Word>> getNRandomWordsFilteredByTagNamesAndUntaggedAndDifficultiesList
//            (List<Difficulty> diffs, List<String> tagNames, int n);
//
//
//    @Query("select * from Word " +
//            "where word.difficulty in (:diffs) " +
//            "order by random() limit :n")
//    LiveData<List<Word>> getNRandomWordsFilteredByDifficultiesList
//                            (List<Difficulty> diffs, int n);





}
