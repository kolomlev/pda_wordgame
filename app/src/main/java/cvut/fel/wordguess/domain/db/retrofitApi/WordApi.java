package cvut.fel.wordguess.domain.db.retrofitApi;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface WordApi {

    @GET("words/beginner/random/{N}")
    Call<ResponseBody> loadNBeginnerWords(@Path("N") int n);

    @GET("words/vocab/academic")
    Call<ResponseBody> loadAcademicTaggedWordList();

    @GET("words/vocab/science")
    Call<ResponseBody> loadScienceTaggedWordList();

    @GET("words/vocab/music")
    Call<ResponseBody> loadMusicTaggedWordList();


}
