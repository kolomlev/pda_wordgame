package cvut.fel.wordguess.domain.db.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import cvut.fel.wordguess.domain.Tag;

@Dao
public interface TagDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTag(Tag tag);

    @Query("select * from Tag")
    List<Tag> getAllTags();

    @Query("select * from Tag where Tag.name = :name ")
    Tag getTagByName(String name);

    @Query("select * from Tag")
    LiveData<List<Tag>>getAllTagsAsync();

    @Update
    void updateTag(Tag tag);

    @Delete
    void deleteTag(Tag tag);

    @Query("delete from Tag")
    void deleteAll();

}
