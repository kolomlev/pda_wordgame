package cvut.fel.wordguess.game;

import java.util.List;

import cvut.fel.wordguess.domain.Word;
import lombok.Getter;

public class PlayQuestion {
    @Getter
    private Word word;

    @Getter
    private List<String> options;

    public PlayQuestion(Word word, List<String> options) {
        this.word = word;
        this.options = options;
    }

    public boolean answer(String ans) {
        return this.word.getSelf().equals(ans);
    }

    @Override
    public String toString() {
        return "PlayQuestion{" +
                "word=" + word +
                ", options=" + options +
                '}';
    }
}
