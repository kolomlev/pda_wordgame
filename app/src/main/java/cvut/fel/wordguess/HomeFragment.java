package cvut.fel.wordguess;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

public class HomeFragment extends Fragment {

    private HomeViewModel mViewModel;

    private TextView mAuthorTextView;
    private TextView mQuoteTextView;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // This ViewModel will be alive as long as the main activity is.
        mViewModel = ViewModelProviders.of(this.getActivity()).get(HomeViewModel.class);
        final View view = inflater.inflate(R.layout.home_fragment, container, false);
        mQuoteTextView = view.findViewById(R.id.home_quote);
        mAuthorTextView = view.findViewById(R.id.home_author);

        mViewModel.getQuote().observe(this.getActivity(), quote -> {
            if (quote != null) {
                mQuoteTextView.setText(quote.getContent());
                mAuthorTextView.setText(quote.getAuthor());
            }
        });
        return view;
    }


}
