package cvut.fel.wordguess.profile;

import android.app.Application;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import cvut.fel.wordguess.domain.Game;
import cvut.fel.wordguess.domain.Profile;
import cvut.fel.wordguess.domain.db.MiscRepository;
import cvut.fel.wordguess.domain.db.ProfileRepository;
import lombok.Getter;

public class ProfileViewModel extends AndroidViewModel {
    private final String TAG  = "ProfileVM";

    // Data repos
    private ProfileRepository profileRepository;

    private MiscRepository miscRepository;


    // Stores current user for MyProfile fragment and chosen user for other Profile fragments
    private Profile userToRender;

    private LiveData<List<Game>> gamesOfCurrentUser;

    // Directories, paths constants
    private final File EXTERNAL_FILES_DIR = this.getApplication()
            .getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    private final File PICTURES_DIR = new File(EXTERNAL_FILES_DIR + "/userpics");
    public final String PICTURES_PATH = PICTURES_DIR.getAbsolutePath();

    // States if current picture is enlarged. Functions in reactive way.
    @Getter
    private MutableLiveData<Boolean> isEnlargedPicture;

    public ProfileViewModel(@NonNull Application application) {
        super(application);
        PICTURES_DIR.mkdirs(); // Creating the directory for userpics

        profileRepository = new ProfileRepository(this.getApplication());
        miscRepository    = new MiscRepository(this.getApplication());

        isEnlargedPicture = new MutableLiveData<>();
        isEnlargedPicture.postValue(false);
    }

    // Finds the user
    public Profile getUserToRender(String username) {
        try {
            this.userToRender = profileRepository.getUserByUsernameSync(username);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this.userToRender;
    }

    public Profile getUserToRender() {
        return this.userToRender;
    }

    public void setUserToRender(Profile profileLiveData) {
        this.userToRender = profileLiveData;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public File createUserpicFile() {

        // Attempt to delete older picture files to free up space
        File[] oldPics = PICTURES_DIR.listFiles(file ->
                file.getName().contains(userToRender.getUsername()));
        for (File oldPic : oldPics) {
            oldPic.delete();
        }

        String ts = new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String picFileName = "IMG_" +  userToRender.getUsername() + "_" + ts;
        File f = new File(PICTURES_PATH + "/" + picFileName + ".jpg");
        Log.d(TAG, f.getAbsolutePath());
        return f;
    }

    public void updateUserpicPath(String absPath) {

        // Updating user profile with userpic path reference
        Profile profile = userToRender;
        assert profile != null;
        profile.setUserpicPath(absPath);
        profileRepository.updateUser(profile);
    }

    public LiveData<List<Game>> getGamesOfCurrentUser(String username) {
        if (this.gamesOfCurrentUser == null) {
            this.gamesOfCurrentUser = miscRepository.getGamesForUsernameAsync(username);
        }
        return this.gamesOfCurrentUser;
    }

    public boolean setAttribute(String attribute, String after) {
        boolean changed = false;
        if (attribute.equals("name")) {
            changed = !userToRender.getName().equals(after);
            if (changed) userToRender.setName(after);
        } else if (attribute.equals("surname")) {
            changed = !userToRender.getSurname().equals(after);
            if (changed) userToRender.setSurname(after);
        }

        if (changed) {
            profileRepository.updateUser(userToRender);
        }
        return changed;
    }
}
