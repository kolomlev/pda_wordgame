package cvut.fel.wordguess;

import android.app.Application;

import java.util.concurrent.ExecutionException;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import cvut.fel.wordguess.domain.Profile;
import cvut.fel.wordguess.domain.db.ProfileRepository;
import lombok.Getter;

public class MainViewModel  extends AndroidViewModel {

    private ProfileRepository profileRepository;

    @Getter
    private Profile currentUser;


    public MainViewModel(@NonNull Application application) {
        super(application);
        profileRepository = new ProfileRepository(this.getApplication());
    }

    public void setCurrentUser(String username) {
        try {
            currentUser = profileRepository.getUserByUsernameSync(username);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
