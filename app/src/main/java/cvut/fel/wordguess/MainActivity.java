package cvut.fel.wordguess;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;
import cvut.fel.wordguess.auth.LoginActivity;
import cvut.fel.wordguess.dictionary.AddWordFragment;
import lombok.Getter;

public class MainActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();

    @Getter
    private MainViewModel mainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setupViewModel();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Check if first start indeed
        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        boolean firstStart = prefs.getBoolean("firstStart", true);

        if (firstStart)
            initialManual();

        Toolbar tb = findViewById(R.id.main_toolbar);
        setSupportActionBar(tb);
        setupBottomNavMenu();


    }


    private void setupViewModel() {
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        String receivedUsername = getIntent().getStringExtra("username");
        // Sets current user, received as Intent field
        if (receivedUsername != null) {
            mainViewModel.setCurrentUser(receivedUsername);
        }
    }

    /* Initial manual */
    // Displays manual on first app run
    private void initialManual() {
        // Go to manual
        Intent goToMan = new Intent(this, HintActivity.class);
        goToMan.putExtra("username", getIntent().getStringExtra("username"));
        startActivity(goToMan, ActivityOptions.makeBasic().toBundle());

    }


    /*Action bar*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_opt_topbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.log_out) {
            // Single option - logout - for main activity and all fragments inside
            Intent goToLogin = new Intent(this, LoginActivity.class);
            startActivity(goToLogin);
        }
        return super.onOptionsItemSelected(item);
    }


    /* Navigation */
    private void setupBottomNavMenu() {
        NavHostFragment host = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.hostFragment);
        assert host != null;
        NavController navController = host.getNavController();
        BottomNavigationView view = findViewById(R.id.bottom_nav_view);
        NavigationUI.setupWithNavController(view, navController);
    }

}
