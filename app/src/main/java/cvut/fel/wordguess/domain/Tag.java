package cvut.fel.wordguess.domain;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import lombok.Getter;
import lombok.Setter;

@Entity(indices = {@Index("name")})
public class Tag {

    @PrimaryKey
    @NonNull
    @Getter
    @Setter
    private String name;


    public static Tag createTag(String tagName) {
        Tag t = new Tag();
        t.setName(tagName);
        return t;
    }

}
