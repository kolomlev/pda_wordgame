package cvut.fel.wordguess.domain.db.dao;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import cvut.fel.wordguess.domain.Quote;

@Dao
public interface QuoteDao {

    @Query("select * from quote")
    List<Quote> getAllQuotes();

    @Query("select * from quote order by quote.dateAdded desc limit 1")
    Quote getLatestQuote();

    @Insert
    void insert(Quote q);

    @Delete
    void delete(Quote q);

}
