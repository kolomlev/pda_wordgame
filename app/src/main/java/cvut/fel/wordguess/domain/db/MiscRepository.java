package cvut.fel.wordguess.domain.db;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.lifecycle.LiveData;
import cvut.fel.wordguess.domain.Game;
import cvut.fel.wordguess.domain.Quote;
import cvut.fel.wordguess.domain.Tag;


// Repository for small entities (Tag, Difficulty)
public class MiscRepository {
    private final String TAG = MiscRepository.class.getSimpleName();

    private AppDatabase db;

    public MiscRepository(Context context) {
        db = AppDatabase.getDatabase(context);
    }

    /*      TAG      */
    @SuppressLint("StaticFieldLeak")
    public void insertTag(final Tag tag) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                db.tagModel().insertTag(tag);
                return null;
            }
        }.execute();
    }

    @SuppressLint("StaticFieldLeak")
    public Tag getTagByName(String name) throws ExecutionException, InterruptedException {
        return new AsyncTask<Void, Void, Tag>() {
            @Override
            protected Tag doInBackground(Void... voids) {
                return db.tagModel().getTagByName(name);
            }
        }.execute().get();
    }


    @SuppressLint("StaticFieldLeak")
    public List<Tag> getAllTags() throws ExecutionException, InterruptedException {
        return new AsyncTask<Void, Void, List<Tag>>() {
            @Override
            protected List<Tag> doInBackground(Void... voids) {
                return db.tagModel().getAllTags();
            }
        }.execute().get();
    }

    public LiveData<List<Tag>> getAllTagsAsync() {
        return db.tagModel().getAllTagsAsync();
    }

    /* GAME */


    @SuppressLint("StaticFieldLeak")
    public void insertGame(final Game g) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                db.gameModel().insert(g);
                Log.d(TAG, "Inserted game: " + g.toString());
                return null;
            }
        }.execute();
    }

    public LiveData<List<Game>> getAllGames() {
        return db.gameModel().getAllGames();
    }


    @SuppressLint("StaticFieldLeak")
    public List<Game> getGamesForUsernameSync(String username) throws ExecutionException, InterruptedException {
        return new AsyncTask<Void, Void, List<Game>>() {
            @Override
            protected List<Game> doInBackground(Void... voids) {
                return db.gameModel().getAllGamesForUsernameSync(username);
            }
        }.execute().get();
    }

    public LiveData<List<Game>> getGamesForUsernameAsync(String username){
        return db.gameModel().getAllGamesForUsernameAsync(username);
    }


    /* QUOTE */

    @SuppressLint("StaticFieldLeak")
    public List<Quote> getAllQuotes() throws ExecutionException, InterruptedException {
        return new AsyncTask<Void, Void, List<Quote>>() {
            @Override
            protected List<Quote> doInBackground(Void... voids) {
                return db.quoteModel().getAllQuotes();
            }
        }.execute().get();
    }

    @SuppressLint("StaticFieldLeak")
    public void insertQuote(final Quote q) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                db.quoteModel().insert(q);
                Log.d(TAG, "Inserted quote: " + q.toString());
                return null;
            }
        }.execute();
    }


    @SuppressLint("StaticFieldLeak")
    public Quote getLatestQuote() throws ExecutionException, InterruptedException {
        return new AsyncTask<Void, Void, Quote>() {
            @Override
            protected Quote doInBackground(Void... voids) {
                return db.quoteModel().getLatestQuote();
            }
        }.execute().get();
    }


}
