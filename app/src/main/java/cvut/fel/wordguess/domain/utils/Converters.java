package cvut.fel.wordguess.domain.utils;


import java.util.Date;

import androidx.room.TypeConverter;


public class Converters {


    @TypeConverter
    public Date getDate(Long l) {
        return l == null ? null : new Date(l);
    }


    @TypeConverter
    public Long getDate(Date d) {
        return d == null ? null : d.getTime();
    }

}
