package cvut.fel.wordguess.domain.db;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;
import cvut.fel.wordguess.domain.Difficulty;
import cvut.fel.wordguess.domain.Game;
import cvut.fel.wordguess.domain.Profile;
import cvut.fel.wordguess.domain.Quote;
import cvut.fel.wordguess.domain.Tag;
import cvut.fel.wordguess.domain.Word;
import cvut.fel.wordguess.domain.db.dao.GameDao;
import cvut.fel.wordguess.domain.db.dao.ProfileDao;
import cvut.fel.wordguess.domain.db.dao.QuoteDao;
import cvut.fel.wordguess.domain.db.dao.TagDao;
import cvut.fel.wordguess.domain.db.dao.WordDao;
import cvut.fel.wordguess.domain.db.retrofitApi.WordApi;
import cvut.fel.wordguess.domain.utils.Converters;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;


// 11 - Quote added
// 10 - Game added
// 9 - Word: Tag - remove @NonNull, add @Nullable
// 8 - Word: delete ID, change PK to Word.self
// 6, 7 - Tag: constraints added
// 5 - Tag: added Tag entity
// 4 - Profile: alter profile - add userpic_id as "text"
@Database(entities = {Word.class, Profile.class, Tag.class, Game.class, Quote.class}, version = 11, exportSchema = false)
@TypeConverters({Difficulty.class, Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    // API url
    private static final String BASE_URL = "https://my-project-1493752659302.appspot.com";

    // DB instance
    private static volatile AppDatabase INSTANCE;


    public abstract WordDao wordModel();

    public abstract ProfileDao profileModel();

    public abstract TagDao tagModel();

    public abstract GameDao gameModel();

    public abstract QuoteDao quoteModel();


    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, "local_db")
                            .addCallback(initialPopulatingCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public static void updateWordBase() {
        Log.d("DB", "Update word base called");
        try {
            if (INSTANCE != null) {
                new fetchWordsTask(INSTANCE).execute();
            }
        } catch (Exception e) {
            Log.d("DB", "Exception while updating word base");
            e.printStackTrace();
        }
    }

    /* DB INITIALIZE CALLBACKS */
    private static RoomDatabase.Callback initialPopulatingCallback =
            new RoomDatabase.Callback() {

                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    new fetchTags(INSTANCE).execute();
                }
            };

    private static class fetchTags extends AsyncTask<Void, Void, Void> {


        private final TagDao tagDao;

        fetchTags(AppDatabase db) {
            tagDao = db.tagModel();
        }


        @Override
        protected Void doInBackground(Void... voids) {
            // Initial tags list
            List<String> initTags =
                    Arrays.asList("Untagged", "Animals", "Movies", "Programming");
            for (String initTag : initTags) {
                if (tagDao.getTagByName(initTag) == null) {
                    Tag t = new Tag();
                    t.setName(initTag);
                    tagDao.insertTag(t);
                }
            }
            return null;
        }
    }

    private static class fetchWordsTask extends AsyncTask<Void, Void, Void> {

        private final WordDao wordDao;
        private final TagDao tagDao;

        private List<Tag> currentTags;

        fetchWordsTask(AppDatabase db) {
            wordDao = db.wordModel();
            tagDao = db.tagModel();
        }

        @Override
        protected Void doInBackground(final Void... params) {

            // Check if database already populated


            // Fetch persisted tags
            currentTags = tagDao.getAllTags();

            // Init params
            List<Word> fetched;
            Call<ResponseBody> call;


            // Add beginner words
//            call = api.loadNBeginnerWords(100);
            fetched = fetchNRandomBeginnerWords(100);
            for (Word w : fetched) {
                wordDao.insert(w);
            }


            // Add academic words
            call = api.loadAcademicTaggedWordList();
            fetched = fetchWordsFromVocabulary(call);
            for (Word w : fetched) {
                wordDao.insert(w);
            }


            // Add science words
            call = api.loadScienceTaggedWordList();
            fetched = fetchWordsFromVocabulary(call);
            for (Word w : fetched) {
                wordDao.insert(w);
            }

            // Add music words
            call = api.loadMusicTaggedWordList();
            fetched = fetchWordsFromVocabulary(call);
            for (Word w : fetched) {
                wordDao.insert(w);
            }

            return null;
        }

        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).build();
        WordApi api = retrofit.create(WordApi.class);

        private List<Word> fetchNRandomBeginnerWords(int n) {
            Tag t = tagDao.getTagByName("Untagged");

            Call<ResponseBody> call = api.loadNBeginnerWords(n);
            List<Word> fetched = new ArrayList<>();
            try {
                String resp = call.execute().body().string();
                JSONArray arr = new JSONArray(resp);

                for (int i = 0; i < arr.length(); i++) {
                    JSONObject raw = arr.getJSONArray(i).getJSONObject(1);
                    Word w = Word.create(raw.getString("self"),
                            raw.getString("definition"),
                            Difficulty.Beginner,
                            new Date(), null, t);
                    fetched.add(w);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            return fetched;
        }


        private List<Word> fetchWordsFromVocabulary(Call<ResponseBody> call) {
            List<Word> fetched = new ArrayList<>();
            try {
                String resp = call.execute().body().string();
                JSONArray arr = new JSONArray(resp);
                for (int i = 0; i < arr.length(); i++) {
                    JSONObject raw = arr.getJSONObject(i);

                    // Get the tag from received JSON. Look it up in existing tag list

                    Tag t = null;
                    String rawTagName = raw.getString("tag");
                    if (rawTagName == null || rawTagName.equals(""))
                        rawTagName = "Untagged";
                    for (Tag temp : currentTags) {
                        if (temp.getName().equals(rawTagName)) {
                            t = temp;
                            break;
                        }
                    }

                    // If it's not there, create new tag.
                    if (t == null) {
                        Tag newTag = new Tag();
                        newTag.setName(rawTagName);
                        tagDao.insertTag(newTag);
                        t = newTag;
                    }


                    Word w = Word.create(raw.getString("self"),
                            raw.getString("definition"),
                            Difficulty.valueOf(raw.getString("level")),
                            null, null, t);
                    fetched.add(w);
                }

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return fetched;
        }


    }
}
