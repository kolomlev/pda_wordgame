package cvut.fel.wordguess.dictionary;

import android.annotation.TargetApi;
import android.app.Application;
import android.os.Build;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import cvut.fel.wordguess.domain.Difficulty;
import cvut.fel.wordguess.domain.Tag;
import cvut.fel.wordguess.domain.Word;
import cvut.fel.wordguess.domain.db.AppDatabase;
import cvut.fel.wordguess.domain.db.MiscRepository;
import cvut.fel.wordguess.domain.db.dao.WordDao;
import lombok.Getter;


// TODO: Add showing untagged words as a category
public class DictionaryViewModel extends AndroidViewModel {
    private final String TAG = this.getClass().getName();

    private MiscRepository miscRepository;
    private WordDao wordModel;

    @Getter
    private MutableLiveData<WordFilter> filter = new MutableLiveData<>(new WordFilter());

    @Getter
    private LiveData<List<Word>> currentWords;

    @Getter
    private MutableLiveData<List<Word>> wordBase = new MutableLiveData<>();

    @TargetApi(Build.VERSION_CODES.N)
    public DictionaryViewModel(@NonNull Application application) {
        super(application);
        miscRepository = new MiscRepository(this.getApplication());
        wordModel = AppDatabase.getDatabase(this.getApplication()).wordModel();
        //  List of current words to display reacts to taglist changes
        currentWords = Transformations.switchMap(filter, input -> {

            // If not-init input group, return all words
            if (input == null) {
                return wordModel.getAllWordsSorted();
            }

            List<Difficulty> chosenDiffs = new ArrayList<>();
            // If not init or all diffs chosen
            if (input.currentDifficulty == null || input.currentDifficulty == Difficulty.All) {
                chosenDiffs.addAll(Arrays.asList(
                        Difficulty.Beginner, Difficulty.Intermediate, Difficulty.Advanced
                ));
            } else {
                // Some diff chosen
                chosenDiffs.add(input.currentDifficulty);
            }

            List<String> chosenTagNames = input.tagFilter.stream()
                    .map(Tag::getName)
                    .collect(Collectors.toList());
            // TODO: Simplify the queries
            if (chosenTagNames.size() == 0) {
                // No tags chosen => return all
                return wordModel.getAllWordsByDifficultiesListSorted(chosenDiffs);
            } else {
//                // Untagged is actually null tag
//                if (chosenTagNames.contains("Untagged")) {
//                    if (chosenTagNames.size() == 1) {
//                        // Untagged only
//                        return wordModel.getUntaggedWordsSorted(chosenDiffs);
//                    } else {
//                        // Untagged + Tagged
//                        return wordModel.getUntaggedAndTaggedWordsSorted(chosenDiffs, chosenTagNames);
//                    }
//                } else {
//                    // Tagged only
//                    return wordModel.getTaggedWordsSorted(chosenTagNames, chosenDiffs);
//                }

                return wordModel.getTaggedWordsSorted(chosenTagNames, chosenDiffs);
            }

        });

    }

    /* CALLED FROM FRAGMENT*/
    List<Tag> getTags() throws ExecutionException, InterruptedException {
        return miscRepository.getAllTags();
    }

    LiveData<List<Tag>> getTagsAsync() {
        return miscRepository.getAllTagsAsync();
    }

    void addTag(Tag tag) {
        if (tag != null) {
            filter.setValue(filter.getValue().addTag(tag));
        }
    }

    void removeTag(Tag tag) {
        filter.setValue(filter.getValue().removeTag(tag));
    }

    void setDifficulty(Difficulty difficulty) {
        filter.setValue(filter.getValue().setDifficulty(difficulty));
    }
    /* **** */


    // Filter using Builder concept
    private class WordFilter {

        List<Tag> tagFilter = new ArrayList<>();

        Difficulty currentDifficulty;

        WordFilter addTag(Tag tag) {
            tagFilter.add(tag);
            return this;
        }

        WordFilter removeTag(Tag tag) {
            tagFilter.remove(tag);
            return this;
        }

        WordFilter setDifficulty(Difficulty diff) {
            currentDifficulty = diff;
            return this;
        }

        @Override
        public String toString() {
            return "WordFilter{" +
                    "tagFilter=" + tagFilter +
                    ", currentDifficulty=" + currentDifficulty +
                    '}';
        }
    }

}
