package cvut.fel.wordguess.dictionary;


import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;
import cvut.fel.wordguess.MainActivity;
import cvut.fel.wordguess.R;
import cvut.fel.wordguess.domain.Difficulty;
import cvut.fel.wordguess.domain.Profile;
import cvut.fel.wordguess.domain.Tag;
import cvut.fel.wordguess.domain.Word;
import cvut.fel.wordguess.domain.db.MiscRepository;
import cvut.fel.wordguess.domain.db.WordRepository;
import cvut.fel.wordguess.domain.utils.Validation;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddWordFragment extends DialogFragment {
    private final String TAG = AddWordFragment.class.getSimpleName();

    /* VIEWS */
    private Spinner spinnerDiff;
    private AutoCompleteTextView tagACTV;
    private Button addBt;
    private Button discardBt;

    private TextInputLayout wordLayout;
    private TextInputEditText wordContent;

    private TextInputLayout defLayout;
    private TextInputEditText defContent;

    /* DB */
    private MiscRepository miscRepository;
    private WordRepository wordRepository;

    public AddWordFragment() {
        // Required empty public constructor
    }

    public static AddWordFragment newInstance(String title) {

        AddWordFragment fragment = new AddWordFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_add_word, container, false);

        //init db
        miscRepository = new MiscRepository(this.getContext());
        wordRepository = new WordRepository(this.getContext());


        initViews(v);
        initSpinnerDiff();
        // DB query to get all tags
        initSpinnerTag();
        // Buttons listeners
        initButtons(v);


        return v;
    }

    private void initViews(View v) {
        discardBt = v.findViewById(R.id.add_word_bt_discard);
        addBt = v.findViewById(R.id.add_word_bt_add);

        wordLayout = v.findViewById(R.id.add_word_layout_word);
        wordContent = v.findViewById(R.id.add_word_self);

        defLayout = v.findViewById(R.id.add_word_layout_definition);
        defContent = v.findViewById(R.id.add_word_definition);

        spinnerDiff = v.findViewById(R.id.add_word_difficulty);
        tagACTV = v.findViewById(R.id.add_word_tag);

        getDialog().getWindow()
                .setSoftInputMode
                        (WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

    }

    private void initButtons(View v) {

        discardBt.setOnClickListener(v1 -> {
            this.dismiss();
        });

        addBt.setOnClickListener(v1 -> {
            tryAddNewWord();
        });
    }


    private void tryAddNewWord() {
        if (Validation.validateWord(wordContent, wordLayout) &&
                Validation.validateDefinition(defContent, defLayout)) {

            String word = wordContent.getText().toString().toLowerCase();
            String definition = defContent.getText().toString().toLowerCase();


            // Get raw chosen string values of difficulty and tag
            String diffChosenValue = ((TextView) spinnerDiff.getChildAt(0))
                    .getText().toString();
            String tagChosenValue = tagACTV.getText().toString();
            if (tagChosenValue.equals(""))
                tagChosenValue = "Untagged";

            // Query for tag
            try {
                // Get tag
                Tag tag = miscRepository.getTagByName(tagChosenValue);

                // If null, create new tag from entered string
                if (tag == null) {
                    tag = new Tag();
                    tag.setName(tagChosenValue);
                    miscRepository.insertTag(tag);
                }

                Difficulty difficulty = Difficulty.valueOf(diffChosenValue);
                Profile currentUser = ((MainActivity) getActivity())
                        .getMainViewModel()
                        .getCurrentUser();

                // By default, just rewrite the word if it already exists.
                Word w = Word.create(word, definition, difficulty, new Date(), currentUser, tag);
                wordRepository.insertWord(w);
                this.dismiss();
                Toast.makeText(this.getContext(), "Word " + word + " has been added",
                        Toast.LENGTH_SHORT).show();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    private void initSpinnerDiff() {

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this.getContext(),
                R.array.diffs_dict_add,
                android.R.layout.simple_spinner_item
        );
        spinnerDiff.setAdapter(adapter);
        spinnerDiff.setOnItemSelectedListener(enlargeSelectedItemListener);
    }

    @TargetApi(Build.VERSION_CODES.N)
    private void initSpinnerTag() {
        // Get tags from db and create adapter
        List<Tag> tags = null;
        try {
            tags = miscRepository.getAllTags();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Adding tags to adapter
        String[] tagsSuggestions = new String[(tags != null) ? tags.size() : 0];
        if (tags != null) {
            for (int i = 0; i < tags.size(); i++) {
                tagsSuggestions[i] = tags.get(i).getName();
            }
        }
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<>(this.getContext(),
                android.R.layout.simple_spinner_item,
                tagsSuggestions);
        // Setting adapter
        tagACTV.setAdapter(adapter);
    }


    // Simple enlarge selected text listener
    private AdapterView.OnItemSelectedListener
            enlargeSelectedItemListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            View chosen = parent.getChildAt(0);
            if (chosen != null) {
                ((TextView) chosen).setTextSize(20);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

}
