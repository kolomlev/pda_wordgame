package cvut.fel.wordguess.profile;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Locale;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import cvut.fel.wordguess.R;
import cvut.fel.wordguess.domain.Game;
import cvut.fel.wordguess.domain.Profile;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private TextView usernameTv;
    private TextView nameTv;
    private TextView surnameTv;
    private TextView dateTv;

    private TextView gamesPlayedTV;
    private TextView wordsGuessedTV;

    private ImageView smallPic;
    private ImageView bigPic;

    private ConstraintLayout layout;


    // Alive as long as the fragment, so different from the MyProfile's VM
    private ProfileViewModel profileViewModel;

    private Profile profile;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_fragment, container, false);

        // Initializing fragment's VM
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);

        // Username passed from clicked item
        String username = ProfileFragmentArgs.fromBundle(getArguments()).getUsername();
        // Init the views
        initViews(v);

        profile = profileViewModel.getUserToRender(username);
        renderBasicInfo();
        renderStats();

        //Listeners for enlarging the userpic
        smallPic.setOnClickListener(mPicEnlargeClickListener);
        bigPic.setOnClickListener(mPicEnlargeClickListener);

        // Toggling the visibility on the userpic click
        profileViewModel.getIsEnlargedPicture().observe(this, changedBool -> {
            if (changedBool) {
                layout.setVisibility(View.INVISIBLE);
                bigPic.setVisibility(View.VISIBLE);
            } else {
                layout.setVisibility(View.VISIBLE);
                bigPic.setVisibility(View.INVISIBLE);
            }
        });

        return v;
    }


    private void renderBasicInfo() {
        //Rendering user info
        usernameTv.setText(profile.getUsername());
        nameTv.setText(profile.getName());
        surnameTv.setText(profile.getSurname());

        dateTv.setText(
                new SimpleDateFormat("yyyy-MM-dd", Locale.US)
                        .format(profile.getDateRegistered())
        );

        if (profile.getUserpicPath() != null) {
            final File file = new File(profile.getUserpicPath());
            if (file.exists()) {
                Glide.with(this).load(file).into(smallPic);
                Glide.with(this).load(file).into(bigPic);
            }
        }

    }


    private void renderStats() {
        profileViewModel.getGamesOfCurrentUser(profile.getUsername()).observe(getActivity(), list -> {
            if (list == null || list.size() == 0) {
                gamesPlayedTV.setText(String.valueOf(0));
                wordsGuessedTV.setText(String.valueOf(0));
            } else {
                gamesPlayedTV.setText(String.valueOf(list.size()));

                int wordsGuessed = 0;
                for (Game g : list) {
                    wordsGuessed += g.getWordsGuessed();
                }

                wordsGuessedTV.setText(String.valueOf(wordsGuessed));
            }
        });
    }


    private void initViews(View v) {
        //Getting the views
        usernameTv = v.findViewById(R.id.profile_username);
        nameTv = v.findViewById(R.id.profile_name);
        surnameTv = v.findViewById(R.id.profile_surname);
        dateTv = v.findViewById(R.id.profile_date);

        smallPic = v.findViewById(R.id.profile_pic_small);
        bigPic = v.findViewById(R.id.profile_pic_enlarged);

        // Layout of the fragment
        layout = v.findViewById(R.id.profile_layout);

        gamesPlayedTV = v.findViewById(R.id.profile_games);
        wordsGuessedTV = v.findViewById(R.id.profile_words_guessed);


    }


    private View.OnClickListener mPicEnlargeClickListener = v -> handleImageClick(v);

    private void handleImageClick(View v) {
        profileViewModel.getIsEnlargedPicture()
                .postValue(!profileViewModel.getIsEnlargedPicture().getValue());
    }


}
