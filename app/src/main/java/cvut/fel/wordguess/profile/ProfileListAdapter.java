package cvut.fel.wordguess.profile;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import cvut.fel.wordguess.R;
import cvut.fel.wordguess.StatsFragment;
import cvut.fel.wordguess.domain.Profile;

public class ProfileListAdapter
        extends RecyclerView.Adapter
        <ProfileListAdapter.ProfileListHolder> {

    private List<Profile> mProfiles = new ArrayList<>();

    // This reference will navigate to Profile fragment of clicked user
    private StatsFragment statsFragment;

    public ProfileListAdapter(StatsFragment statsFragment) {
        this.statsFragment = statsFragment;
    }

    @NonNull
    @Override
    public ProfileListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.stats_profile_item, parent, false);
        return new ProfileListHolder(view);
    }

    @Override
    public int getItemCount() {
        return mProfiles == null ? 0 : mProfiles.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileListHolder holder, int position) {

        if (mProfiles != null) {
            Profile currentProfile = mProfiles.get(position);
            int scores = statsFragment.getMStatsViewModel()
                    .getScoresForUsername(currentProfile.getUsername());
            holder.name.setText(currentProfile.getName());
            holder.surname.setText(currentProfile.getSurname());
            holder.scores.setText(String.valueOf(scores));
            // Render useric if it exists
            if (currentProfile.getUserpicPath() != null) {
                Glide.with(statsFragment.getContext())
                        .load(currentProfile.getUserpicPath())
                        .into(holder.image);
            } else {
                Glide.with(statsFragment.getContext())
                        .load(R.drawable.question_mark_material)
                        .into(holder.image);
            }
            // Click on the whole view navigates to it's profile fragment
            holder.parent.setOnClickListener(v -> handleProfileItemClick(currentProfile));
        }
    }


    public void setmProfiles(List<Profile> profiles) {
        this.mProfiles = profiles;
        notifyDataSetChanged(); // TODO: replace later
    }


    private void handleProfileItemClick(Profile p) {
        statsFragment.handleProfileItemClick(p);
    }


    class ProfileListHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name;
        TextView surname;
        TextView scores;

        RelativeLayout parent;


        public ProfileListHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.stats_item_pic);
            name = itemView.findViewById(R.id.stats_item_name);
            surname = itemView.findViewById(R.id.stats_item_surname);
            scores = itemView.findViewById(R.id.stats_item_scores);

            parent = itemView.findViewById(R.id.stats_item_parent_layout);

        }
    }

}
