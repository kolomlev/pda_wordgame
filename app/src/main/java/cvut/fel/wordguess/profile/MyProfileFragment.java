package cvut.fel.wordguess.profile;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import cvut.fel.wordguess.MainViewModel;
import cvut.fel.wordguess.R;
import cvut.fel.wordguess.domain.Game;
import cvut.fel.wordguess.domain.Profile;

import static android.app.Activity.RESULT_OK;

public class MyProfileFragment extends Fragment {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final String TAG = MyProfileFragment.class.getSimpleName();
    // Views
    private TextView usernameTv;
    private EditText nameTv;
    private EditText surnameTv;
    private TextView dateTv;

    private ImageView smallPic;
    private ImageView bigPic;
    private ImageButton addPicBt;

    private TextView gamesPlayedTV;
    private TextView wordsGuessedTV;

    private ConstraintLayout layout;
    private RelativeLayout outerLayout;

    // Alive as long as parent activity
    private ProfileViewModel profileViewModel;

    private Profile profile;

    public static MyProfileFragment newInstance() {
        return new MyProfileFragment();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.my_profile_fragment, container, false);

        // Initialize fragment VM
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        // Get main activity VM, that contains info about current user
        MainViewModel mainViewModel = ViewModelProviders.of(getActivity())
                .get(MainViewModel.class);
        // Set this fragment's VM LiveData<Profile> to point to the Main Activity's one
        profileViewModel.setUserToRender(mainViewModel.getCurrentUser());
        profile = profileViewModel.getUserToRender();

        initViews(view);
        renderBasicInfo();
        renderStats();
        setOnTextChangeListeners();

        //Listeners for enlarging the userpic
        smallPic.setOnClickListener(mPicEnlargeClickListener);
        bigPic.setOnClickListener(mPicEnlargeClickListener);

        // Toggling the visibility on the userpic click
        profileViewModel.getIsEnlargedPicture().observe(this, changedBool -> {
            if (changedBool) {
                layout.setVisibility(View.INVISIBLE);
                bigPic.setVisibility(View.VISIBLE);
            } else {
                layout.setVisibility(View.VISIBLE);
                bigPic.setVisibility(View.INVISIBLE);
            }
        });

        // Listeners on add picture click
        addPicBt.setOnClickListener(this::handleAddPictureClick);

        return view;
    }

    private void setOnTextChangeListeners() {
        nameTv.setOnFocusChangeListener(new MyFocusChangeListener("name"));
        surnameTv.setOnFocusChangeListener(new MyFocusChangeListener("surname"));
    }


    private void renderStats() {
        profileViewModel.getGamesOfCurrentUser(profile.getUsername()).observe(getActivity(), list -> {
            if (list == null || list.size() == 0) {
                gamesPlayedTV.setText(String.valueOf(0));
                wordsGuessedTV.setText(String.valueOf(0));
            } else {
                gamesPlayedTV.setText(String.valueOf(list.size()));

                int wordsGuessed = 0;
                for (Game g : list) {
                    wordsGuessed += g.getWordsGuessed();
                }

                wordsGuessedTV.setText(String.valueOf(wordsGuessed));
            }
        });
    }

    private void renderBasicInfo() {
        usernameTv.setText(profile.getUsername());
        nameTv.setText(profile.getName());
        surnameTv.setText(profile.getSurname());

        dateTv.setText(
                new SimpleDateFormat("yyyy-MM-dd", Locale.US)
                        .format(profile.getDateRegistered())
        );
        if (profile.getUserpicPath() != null) {
            final File file = new File(profile.getUserpicPath());
            if (file.exists()) {
                Glide.with(this).load(file).into(smallPic);
                Glide.with(this).load(file).into(bigPic);
            }
        }

    }

    private void initViews(View view) {
        // Getting the views
        usernameTv = view.findViewById(R.id.my_profile_username);
        nameTv = view.findViewById(R.id.my_profile_name);
        surnameTv = view.findViewById(R.id.my_profile_surname);
        dateTv = view.findViewById(R.id.my_profile_date);

        smallPic = view.findViewById(R.id.my_profile_pic_small);
        bigPic = view.findViewById(R.id.my_profile_pic_enlarged);

        addPicBt = view.findViewById(R.id.my_profile_bt_add_pic);

        gamesPlayedTV = view.findViewById(R.id.my_profile_games);
        wordsGuessedTV = view.findViewById(R.id.my_profile_words_guessed);

        // Outer layout
        outerLayout = view.findViewById(R.id.outer_layout);
        // Layout of the fragment
        layout = view.findViewById(R.id.my_profile_layout);
    }


    /* IMAGE ENLARGE */
    private void handleImageClick(View v) {
        profileViewModel.getIsEnlargedPicture()
                .postValue(!profileViewModel.getIsEnlargedPicture().getValue());
    }

    private View.OnClickListener mPicEnlargeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            handleImageClick(v);
        }
    };


    /* IMAGE ADDING */
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void handleAddPictureClick(View v) {
        dispatchTakePictureIntent();
    }

    private File photoFile;

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void dispatchTakePictureIntent() {
        Intent takePicIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePicIntent.resolveActivity(getActivity().getPackageManager()) != null) {

            photoFile = profileViewModel.createUserpicFile();
            if (photoFile != null) {
                Uri photoUri = FileProvider.getUriForFile(this.getContext(),
                        "com.example.android.fileprovider", photoFile);
                takePicIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(takePicIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }


    // When user has taken a picture or cancelled
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            // Photo shot taken
            if (resultCode == RESULT_OK) {
                profileViewModel.updateUserpicPath(photoFile.getAbsolutePath());

                // Cancelled photo shot
            } else {
                Toast.makeText(this.getContext(), "Userpic was not changed",
                        Toast.LENGTH_LONG).show();
            }
        }
        renderBasicInfo();
    }


    /* TEXT EDIT WATCHER */
    // If user changed the text, update the DB with the new values and rerender view.
    class MyFocusChangeListener implements View.OnFocusChangeListener {
        private final String TAG = "Focus listener";
        String attribute;

        public MyFocusChangeListener(String attribute) {
            this.attribute = attribute;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            Log.d(this.TAG, "Focus on: " + v.getClass() + " is now " + hasFocus);

            if (v.getClass().equals(AppCompatEditText.class)) {
                if (!hasFocus) {
                    EditText temp = (AppCompatEditText) v;
                    String after = temp.getText().toString();
                    if (after.length() != 0 &&
                            profileViewModel.setAttribute(attribute, after)) {
                        renderBasicInfo();
                    }
                }
            }
        }
    }

    ;

}
