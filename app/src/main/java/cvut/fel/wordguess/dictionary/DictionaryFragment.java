package cvut.fel.wordguess.dictionary;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import cvut.fel.wordguess.R;
import cvut.fel.wordguess.auth.LoginActivity;
import cvut.fel.wordguess.domain.Difficulty;
import cvut.fel.wordguess.domain.Tag;
import cvut.fel.wordguess.domain.db.AppDatabase;

public class DictionaryFragment extends Fragment {
    private final String TAG = this.getClass().getSimpleName();

    private DictionaryViewModel viewModel;

    private Spinner spinner;

    public static DictionaryFragment newInstance() {
        return new DictionaryFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.dictionary_fragment, container, false);
        viewModel = ViewModelProviders.of(this.getActivity()).get(DictionaryViewModel.class);

        // Init spinner with diffs
        initSpinner(v);
        try {
            initTags(v); // Init chip tags
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        initFab(v); // Init add new word FAB


        // Getting recycler view and setting all necessary params
        RecyclerView recyclerView = v.findViewById(R.id.dict_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        // Creating adapter for recycler
        DictionaryItemListAdapter adapter = new DictionaryItemListAdapter();
        recyclerView.setAdapter(adapter);
        viewModel.getCurrentWords().observe(this, (e) -> {
                    adapter.setWords(e);
                    Log.d(TAG, "Words: " + e);
                }
        );
        // Custom action bar options
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.dict_opt_topbar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Log.d(TAG, "Id: " + id);
        if (id == R.id.log_out) {
            Log.d(TAG, "Id logout: " + R.id.log_out);
            Intent goToLogin = new Intent(this.getContext(), LoginActivity.class);
            startActivity(goToLogin);
        } else if (id == R.id.word_fetch) {
            Log.d(TAG, "Id logout: " + R.id.word_fetch);
            AppDatabase.updateWordBase();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initSpinner(View v) {
        spinner = v.findViewById(R.id.dict_buttons);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(Objects.requireNonNull(this.getContext()), R.array.difficulties, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getChildAt(0) != null) {
                    TextView tv = (TextView) parent.getChildAt(0);
                    (tv).setTextSize(20);

                    viewModel.setDifficulty(Difficulty.valueOf(
                            tv.getText().toString()
                    ));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initTags(View v) throws ExecutionException, InterruptedException {
//        List<Tag> allTags = viewModel.getTags();
        Map<String, Tag> lookupTable = new HashMap<>();
        ChipGroup tagGroup = v.findViewById(R.id.dict_tag_group);

        viewModel.getTagsAsync().observe(this, e -> {
            // remove all old tags
            tagGroup.removeAllViews();
            // Render new tags
            List<Tag> allTags = e;
            for (Tag t : allTags) {

                Chip c = new Chip(v.getContext());
                c.setText(t.getName());
                // Create layout for chip
                ViewGroup.MarginLayoutParams layoutParams = new ViewGroup.MarginLayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(10, 10, 10, 10);
                c.setId(View.generateViewId());
                // Set layout params
                c.setLayoutParams(layoutParams);
                c.setCheckable(true);

                lookupTable.put(t.getName(), t);

                c.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    if (isChecked) {
                        viewModel.addTag(lookupTable.get(buttonView.getText().toString()));
                    } else {
                        viewModel.removeTag(lookupTable.get(buttonView.getText().toString()));
                    }
                });
                tagGroup.addView(c);
            }
        });
    }


    private void initFab(View v) {
        FloatingActionButton fab = v.findViewById(R.id.dict_add_fab);

        fab.setOnClickListener(e -> {
            FragmentManager fm = getFragmentManager();
            AddWordFragment fragment = AddWordFragment.newInstance("Add new word");
            fragment.show(fm, "add_word_fragment");
        });


    }
}
