package cvut.fel.wordguess.domain.utils;

import java.io.Serializable;
import java.util.ArrayList;

import cvut.fel.wordguess.domain.Word;
import lombok.Getter;
import lombok.Setter;

public class WordListWrapper implements Serializable {

    private static final long seialUUID = 1L;

    @Getter
    @Setter
    private ArrayList<Word> words;

    public WordListWrapper(ArrayList<Word> words) {
        this.words = words;
    }
}
