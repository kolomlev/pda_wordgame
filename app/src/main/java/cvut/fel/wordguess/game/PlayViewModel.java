package cvut.fel.wordguess.game;

import android.annotation.TargetApi;
import android.app.Application;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import cvut.fel.wordguess.domain.Difficulty;
import cvut.fel.wordguess.domain.Tag;
import cvut.fel.wordguess.domain.Word;
import cvut.fel.wordguess.domain.db.AppDatabase;
import cvut.fel.wordguess.domain.db.MiscRepository;
import cvut.fel.wordguess.domain.db.dao.WordDao;
import cvut.fel.wordguess.domain.utils.WordListWrapper;
import lombok.Getter;


public class PlayViewModel extends AndroidViewModel {
    private final String TAG = this.getClass().getSimpleName();

    private MiscRepository miscRepository;
    private WordDao wordModel;
    private AppDatabase db;

    private LiveData<List<Word>> wordBase;

    @Getter
    private List<Word> wordsToPlay;

    private List<Difficulty> difficultiesList;
    private List<Tag> tagList;

    private Random random = new Random();

    // lookup with answers
    @Getter
    private HashMap<Word, Boolean> playedWordsTable = new HashMap<>();
    @Getter
    private int wrongCnt = 0;
    @Getter
    private int rightCnt = 0;


    public PlayViewModel(@NonNull Application application) {
        super(application);

        db = AppDatabase.getDatabase(this.getApplication());
        wordModel = db.wordModel();
        miscRepository = new MiscRepository(this.getApplication());

    }

    public void registerWordBaseParams(String[] diffs, String[] tags) {
        // Finding diffs
        difficultiesList = new ArrayList<>();
        for (String d : diffs) {
            difficultiesList.add(Difficulty.valueOf(d));
        }

        // Finding tags
        tagList = new ArrayList<>();
        for (String t : tags) {
            try {
                tagList.add(miscRepository.getTagByName(t));
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public LiveData<List<Word>> getWordBase() {
        if (wordBase == null) {
            loadWordBase();
        }
        return wordBase;
    }


    // Filters the words and sets the words to play
    @TargetApi(Build.VERSION_CODES.N)
    public void registerWordBase(List<Word> words) {
        // Map the tagList to list of names of the tags
        List<String> tagNamesList = tagList.stream().map(Tag::getName)
                .collect(Collectors.toList());
        // Initialize the words list
        this.wordsToPlay = new ArrayList<>();

        // Filter word base
        for (Word w : words) {
            if (tagList.size() == 0 || tagNamesList.contains(w.getTag())) {
                if (difficultiesList.size() == 0 || difficultiesList.contains(w.getDifficulty())) {
                    this.wordsToPlay.add(w);
                }
            }
        }
        Log.d(TAG, "Words ready");
    }

    // TODO: add list bound checking
    public PlayQuestion getNextQuestion() {

        // get random word
        Word word;

        if (wordBase.getValue() == null || wordBase.getValue().size() < 4) {
            return null;
        }

        if (wordsToPlay.size() < 4) {
            return null;
        } else {
            word = wordsToPlay.remove(random.nextInt(wordsToPlay.size()));
        }

        // get random definitions
        List<String> opts = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            int randInd = random.nextInt(wordsToPlay.size());
            opts.add(wordBase.getValue().get(randInd).getSelf());
        }
        return new PlayQuestion(word, opts);
    }

    // Loads all words from DB TODO: May be a problem if the words base gets really big, but for now - OK
    private void loadWordBase() {
        // Empty diffs means select all
        if (difficultiesList.size() == 0) {
            difficultiesList.addAll(Arrays.asList(Difficulty.Beginner,
                    Difficulty.Intermediate,
                    Difficulty.Advanced));
        }
        // Queering is based on chosen tags - 4 cases:
        // Untagged x Untagged and Tagged x All x Tagged
        wordBase = wordModel.getAllWordsSorted();
    }

    public void addQuestionRecord(PlayQuestion currentQuestion, boolean b) {
        playedWordsTable.put(currentQuestion.getWord(), b);
        if (b) {
            rightCnt++;
        } else {
            wrongCnt++;
        }
    }


    public WordListWrapper getRightList() {
        ArrayList<Word> res = new ArrayList<>();
        for (Map.Entry<Word, Boolean> e : playedWordsTable.entrySet()) {
            if (e.getValue()) {
                res.add(e.getKey());
            }
        }
        return new WordListWrapper(res);
    }

    public WordListWrapper getWrongList() {
        ArrayList<Word> res = new ArrayList<>();
        for (Map.Entry<Word, Boolean> e : playedWordsTable.entrySet()) {
            if (!e.getValue()) {
                res.add(e.getKey());
            }
        }
        return new WordListWrapper(res);
    }
}
