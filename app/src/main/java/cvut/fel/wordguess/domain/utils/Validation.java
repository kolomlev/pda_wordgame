package cvut.fel.wordguess.domain.utils;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

public class Validation {
    public final static int USERNAME_MAX_LENGTH = 32;

    public final static int PASSWORD_MAX_LENGTH = 32;

    public final static int NAME_MAX_LENGTH = 32;

    public final static int SURNAME_MAX_LENGTH = 32;


    public static boolean validateUsername(TextInputEditText view, TextInputLayout layout) {
        String value = Objects.requireNonNull(view.getText()).toString();
        boolean res = !value.equals("") && value.length() < USERNAME_MAX_LENGTH;
        if (!res){
            layout.setError("Username must be between 0 and 32 symbols");
            view.setText("");
        } else {
            layout.setError(null);
        }
        return res;
    }


    public static boolean validatePassword(TextInputEditText view, TextInputLayout layout) {
        String value = Objects.requireNonNull(view.getText()).toString();
        boolean res = !value.equals("") && value.length() < PASSWORD_MAX_LENGTH;
        if (!res) {
            layout.setError("Password must be between 0 and 32 symbols");
            view.setText("");
        } else {
            layout.setError(null);
        }
        return res;
    }


    public static boolean validateName(TextInputEditText view, TextInputLayout layout) {
        String value = Objects.requireNonNull(view.getText()).toString();
        boolean res = value.matches("[a-zA-Z]+") && value.length() < NAME_MAX_LENGTH;
        if (!res) {
            layout.setError("Name must be between 0 and 32 literals");
            view.setText("");
        } else {
            layout.setError(null);
        }
        return res;
    }


    public static boolean validateSurname(TextInputEditText view, TextInputLayout layout) {
        String value = Objects.requireNonNull(view.getText()).toString();
        boolean res = value.matches("[a-zA-Z]+") && value.length() < SURNAME_MAX_LENGTH;
        if (!res) {
            layout.setError("Surname must be between 1 and 32 literals");
            view.setText("");
        } else {
            layout.setError(null);
        }
        return res;
    }

    public static boolean validateWord(TextInputEditText view, TextInputLayout layout) {
        String value = Objects.requireNonNull(view.getText()).toString();
        boolean res = !value.equals("") && value.matches("\\w+");
        if (!res) {
            layout.setError("Word can only contain literals or digits");
        } else {
            layout.setError(null);
        }
        return res;
    }

    public static boolean validateDefinition(TextInputEditText view, TextInputLayout layout) {
        String value = Objects.requireNonNull(view.getText()).toString();
        boolean res = !value.equals("") && value.matches("[\\w\\s]+");
        if (!res) {
            layout.setError("Word definition can only contain literals or digits");
        } else {
            layout.setError(null);
        }
        return res;
    }

}
