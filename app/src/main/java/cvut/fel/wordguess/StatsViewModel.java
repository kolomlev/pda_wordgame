package cvut.fel.wordguess;

import android.app.Application;

import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import cvut.fel.wordguess.domain.Game;
import cvut.fel.wordguess.domain.Profile;
import cvut.fel.wordguess.domain.db.MiscRepository;
import cvut.fel.wordguess.domain.db.ProfileRepository;

public class StatsViewModel extends AndroidViewModel {

    private LiveData<List<Profile>> allProfiles = new MutableLiveData<>();
    private ProfileRepository profileRepository;
    private MiscRepository miscRepository;

    public StatsViewModel(@NonNull Application application) {
        super(application);
        this.profileRepository = new ProfileRepository(getApplication());
        this.miscRepository = new MiscRepository(getApplication());
        allProfiles = profileRepository.getAllUsers();
    }


    public LiveData<List<Profile>> getAllProfiles() {
        return allProfiles;
    }

    public int getScoresForUsername(String username) {

        try {
            List<Game> games = miscRepository.getGamesForUsernameSync(username);
            int cnt = 0;
            for (Game g : games) {
                cnt += g.getWordsGuessed() != null ? g.getWordsGuessed() : 0;
            }
            return cnt;
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
