package cvut.fel.wordguess.dictionary;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.chip.Chip;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import cvut.fel.wordguess.R;
import cvut.fel.wordguess.domain.Word;
import lombok.Setter;

public class DictionaryItemListAdapter
        extends RecyclerView.Adapter<DictionaryItemListAdapter.ItemHolder> {


    private final String TAG = "DictItemListAdapter";

    private List<Word> words;

    @Setter
    private int background = 0;

    public DictionaryItemListAdapter() {

    }

    public DictionaryItemListAdapter(int background) {
        this.background = background;
    }

    @NonNull
    @Override
    public ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dictionary_item, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public int getItemCount() {
        return words == null ? 0 : words.size();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull ItemHolder holder, int position) {

        if (words != null) {
            Word currentWord = words.get(position);
//            Log.d(TAG, currentWord.toString());
            holder.word.setText(currentWord.getSelf());
            holder.definition.setText(currentWord.getDefinition());
            holder.tag.setText(currentWord.getTag());

            holder.view.setOnClickListener(l -> {
                Log.d(TAG, "Clicked word: " + holder.word.getText().toString());
                ViewGroup.LayoutParams params = holder.detail.getLayoutParams();
                params.height = params.height == 0 ? ViewGroup.LayoutParams.WRAP_CONTENT : 0;
                holder.detail.setLayoutParams(params);
            });

            // color
            if (this.background != 0) {
                holder.view.setBackgroundColor(this.background);
            }
        }

    }

    public void setWords(List<Word> words) {
        this.words = words;
        notifyDataSetChanged();
    }


    class ItemHolder extends RecyclerView.ViewHolder {

        TextView word;
        TextView definition;
        Chip tag;

        RelativeLayout detail;

        View view;

        ItemHolder(@NonNull View itemView) {
            super(itemView);

            word = itemView.findViewById(R.id.dict_item_word);
            definition = itemView.findViewById(R.id.dict_item_definition);
            tag = itemView.findViewById(R.id.dict_item_tag);
            detail = itemView.findViewById(R.id.dict_item_detail);

            view = itemView;
        }
    }
}
