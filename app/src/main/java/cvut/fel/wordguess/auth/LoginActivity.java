package cvut.fel.wordguess.auth;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import androidx.appcompat.app.AppCompatActivity;
import cvut.fel.wordguess.MainActivity;
import cvut.fel.wordguess.R;
import cvut.fel.wordguess.domain.Profile;
import cvut.fel.wordguess.domain.db.ProfileRepository;
import cvut.fel.wordguess.domain.utils.Validation;

public class LoginActivity extends AppCompatActivity {

    private ProfileRepository profileRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        profileRepository = new ProfileRepository(this);
        TextInputEditText username = findViewById(R.id.login_username);
        // Came from registration
        if (savedInstanceState != null) {
            username.setText(savedInstanceState.getString("username"));
        }

    }

    public void navigateToRegistration(View link) {
        Intent goToReg = new Intent(this, RegisterActivity.class);
        startActivity(goToReg);
    }

    public void attemptLogin(View bt) throws NoSuchAlgorithmException, ExecutionException, InterruptedException {

        TextInputLayout usernameField = findViewById(R.id.login_layout_username);
        TextInputEditText usernameContent = findViewById(R.id.login_username);
        String username = Objects.requireNonNull(usernameContent.getText()).toString();

        TextInputLayout passwordField = findViewById(R.id.login_layout_password);
        TextInputEditText passwordContent = findViewById(R.id.login_password);
        String password = Objects.requireNonNull(passwordContent.getText()).toString();

        // Validating
        if (Validation.validateUsername(usernameContent, usernameField)
                && Validation.validatePassword(passwordContent, passwordField)) {

            // Checking presence of @username
            Profile attemptUser = profileRepository.getUserByUsernameSync(username);

            if (attemptUser == null) {
                // No such user
                Toast.makeText(this, "Username " + username + " does not exist",
                        Toast.LENGTH_SHORT).show();
                usernameContent.setText("");
                passwordContent.setText("");
            } else {

                // Matched user, checking passwords
                MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
                messageDigest.update(password.getBytes());
                String inputEnc = new String(messageDigest.digest());

                if (attemptUser.getPassword().equals(inputEnc)) {
                    // Passwords match
                    Intent goToMainActivity = new Intent(this, MainActivity.class);
                    goToMainActivity.putExtra("username", username);
                    startActivity(goToMainActivity);

                } else {
                    // No match
                    Toast.makeText(this, "Bad password for user " + username,
                            Toast.LENGTH_SHORT).show();
                    passwordContent.setText("");
                }
            }

        }


    }

}
