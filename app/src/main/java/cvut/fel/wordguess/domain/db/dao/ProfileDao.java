package cvut.fel.wordguess.domain.db.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import cvut.fel.wordguess.domain.Profile;

@Dao
public interface ProfileDao {

    @Insert
    void insert(Profile user);

    @Query("select * from Profile")
    LiveData<List<Profile>> getAllUsers();

    @Query("select * from Profile p where p.username like :username")
    LiveData<Profile> getUserByUsername(String username);


    @Query("select * from Profile p where p.username like :username")
    Profile getUserByUsernameSync(String username);

    @Query("select * from Profile p where p.name like :name and p.surname like :surname")
    LiveData<Profile> getUserByNameAndSurname(String name, String surname);

    @Update
    void updateUser(Profile user);

    @Delete
    void deleteUser(Profile user);
}
